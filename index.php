<!DOCTYPE html>
<html lang="en">

<head>
  <title>GYM</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Karla:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
  <!-- <link rel="stylesheet" href="assets/css/custom-button-red.css"> -->
  <link rel="stylesheet" href="assets/css/bootstrap4.css">
  <link rel="stylesheet" href="assets/css/bootstrap4.min.css">
  <link rel="stylesheet" href="assets/css/style.css">
  <link rel="stylesheet" href="assets/css/custom.css">

<style>

</style>

</head>

<body>
  <?php
    include_once 'view/section/section-1.php';
    include_once 'view/section/section-2.php';
    include_once 'view/section/section-3.php';
    // include_once 'view/section/section-4.php';
    include_once 'view/section/section-4-2.php';
    include_once 'view/section/section-4-3.php';
    include_once 'view/section/section-5.php';
  ?>

  <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

  <script>
    AOS.init({
    duration: 1200,
  })

  </script>
</body>

</html>